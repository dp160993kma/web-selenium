package max.demo.conf;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by a418 on 9/17/17.
 */

public class CaseFormatTest {
    @Test
    public void convertsCamelCaseToLowerUnderscore() {
        assertThat(CaseFormat.toLowerUnderscore("HomeControllerTest"))
                .isEqualTo("home_controller_test");
    }
}
