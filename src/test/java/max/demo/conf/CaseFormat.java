package max.demo.conf;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by a418 on 9/17/17.
 */
public final class CaseFormat {

    public static String toLowerUnderscore(String upperCamel) {
        return Stream
                .of(upperCamel.split("(?=[A-Z])"))
                .map(s -> s.toLowerCase())
                .collect(Collectors.joining("_"));
    }

}
