package max.demo.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
/**
 * Created by a418 on 9/17/17.
 */
public class GoogleSearchPage {


    @FindBy(className = "mat-checkbox-inner-container")
    private WebElement divCheckBox;

    @FindBy(className = "custom-element-input[_ngcontent-c3]")
    private WebElement inputNumber;



    private final WebDriver driver;

    public GoogleSearchPage(WebDriver driver) {

        this.driver = driver;
    }

    public WebElement getDivCheckBox() {

        return divCheckBox;
    }

    public WebElement getInputNumber() {

        return inputNumber;
    }

}
