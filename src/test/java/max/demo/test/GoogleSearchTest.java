package max.demo.test;

import com.jayway.restassured.RestAssured;
import max.demo.conf.SeleniumTest;
import max.demo.page.GoogleSearchPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by a418 on 9/17/17.
 */
@RunWith(SpringRunner.class)
@SeleniumTest(driver = ChromeDriver.class, baseUrl = "https://online-anketa.privatbank.ua/OnlineAnketa/?lang=ru&card_type=GOLD")

class FunctionalTest {

    @BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8080);
        }
        else{
            RestAssured.port = Integer.valueOf(port);
        }


        /*String basePath = System.getProperty("server.base");
        if(basePath==null){
            basePath = "/rest-garage-sample/";
        }
        RestAssured.basePath = basePath;*/

        String baseHost = System.getProperty("server.host");
        if(baseHost==null){
            baseHost = "http://online-anketa.privatbank.ua/";
        }
        RestAssured.baseURI = baseHost;

    }

}
public class GoogleSearchTest {

    @Autowired
    WebDriver driver;


    private GoogleSearchPage page;


    @Before
    public void setUp(){

        page = PageFactory.initElements(driver,GoogleSearchPage.class);
    }


    @Test
    public void clickCheckBox(){
        WebElement checkBox = page.getDivCheckBox();
        checkBox.click();
    }

    @Test
    public void searchWord(){
        WebElement searchField = page.getInputNumber();
        searchField.sendKeys("505664828");
        searchField.submit();
    }

    @Test
    public void basicPingTest() {
        given().when().get("/OnlineAnketa/").then().statusCode(200);
    }


}
